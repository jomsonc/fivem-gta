

RegisterCommand('sniper', function(source, args, rawCommand)
    GiveWeaponToPed(
        GetPlayerPed(-1), 
        GetHashKey("WEAPON_SNIPERRIFLE"), 
        999, 
        false, 
        true
    )
    TriggerEvent('chat:addMessage', {
        args = { 'Weapon Command RUN 2!~' }
    })

end)

RegisterCommand('pistol', function(source, args, rawCommand)
    GiveWeaponToPed(
        GetPlayerPed(-1), 
        GetHashKey("WEAPON_APPISTOL"), 
        999, 
        false, 
        true
    )
    TriggerEvent('chat:addMessage', {
        args = { 'Weapon Command RUN 2!~' }
    })

end)
RegisterCommand('revolver', function(source, args, rawCommand)
    GiveWeaponToPed(
        GetPlayerPed(-1), 
        GetHashKey("WEAPON_REVOLVER"), 
        999, 
        false, 
        true
    )
    TriggerEvent('chat:addMessage', {
        args = { 'Weapon Command RUN 2!~' }
    })

end)

RegisterCommand('weapon', function(source, args, rawCommand)
    weaponNames = {  
        "WEAPON_KNIFE", "WEAPON_NIGHTSTICK", "WEAPON_HAMMER", "WEAPON_BAT", "WEAPON_GOLFCLUB",  
        "WEAPON_CROWBAR", "WEAPON_PISTOL", "WEAPON_COMBATPISTOL", "WEAPON_APPISTOL", "WEAPON_PISTOL50",  
        "WEAPON_MICROSMG", "WEAPON_SMG", "WEAPON_ASSAULTSMG", "WEAPON_ASSAULTRIFLE",  
        "WEAPON_CARBINERIFLE", "WEAPON_ADVANCEDRIFLE", "WEAPON_MG", "WEAPON_COMBATMG", "WEAPON_PUMPSHOTGUN",  
        "WEAPON_SAWNOFFSHOTGUN", "WEAPON_ASSAULTSHOTGUN", "WEAPON_BULLPUPSHOTGUN", "WEAPON_STUNGUN", "WEAPON_SNIPERRIFLE",  
        "WEAPON_HEAVYSNIPER", "WEAPON_GRENADELAUNCHER", "WEAPON_GRENADELAUNCHER_SMOKE", "WEAPON_RPG", "WEAPON_MINIGUN",  
        "WEAPON_GRENADE", "WEAPON_STICKYBOMB", "WEAPON_SMOKEGRENADE", "WEAPON_BZGAS", "WEAPON_MOLOTOV",  
        "WEAPON_FIREEXTINGUISHER", "WEAPON_PETROLCAN", "WEAPON_FLARE", "WEAPON_SNSPISTOL", "WEAPON_SPECIALCARBINE",  
        "WEAPON_HEAVYPISTOL", "WEAPON_BULLPUPRIFLE", "WEAPON_HOMINGLAUNCHER", "WEAPON_PROXMINE", "WEAPON_SNOWBALL",  
        "WEAPON_VINTAGEPISTOL", "WEAPON_DAGGER", "WEAPON_FIREWORK", "WEAPON_MUSKET", "WEAPON_MARKSMANRIFLE",  
        "WEAPON_HEAVYSHOTGUN", "WEAPON_GUSENBERG", "WEAPON_HATCHET", "WEAPON_RAILGUN", "WEAPON_COMBATPDW",  
        "WEAPON_KNUCKLE", "WEAPON_MARKSMANPISTOL", "WEAPON_FLASHLIGHT", "WEAPON_MACHETE", "WEAPON_MACHINEPISTOL",  
        "WEAPON_SWITCHBLADE", "WEAPON_REVOLVER", "WEAPON_COMPACTRIFLE", "WEAPON_DBSHOTGUN", "WEAPON_FLAREGUN",  
        "WEAPON_AUTOSHOTGUN", "WEAPON_BATTLEAXE", "WEAPON_COMPACTLAUNCHER", "WEAPON_MINISMG", "WEAPON_PIPEBOMB",  
        "WEAPON_POOLCUE", "WEAPON_SWEEPER", "WEAPON_WRENCH"  
    };  

    for names = 1, 55 do
        GiveWeaponToPed(GetPlayerPed(-1), GetHashKey(weaponNames[names]), 999, false, false)
      end

    TriggerEvent('chat:addMessage', {
        args = { 'Weapon Command RUN ALL!~' }
    })

end)


RegisterCommand('killall', function(source, args, rawCommand)
    SetEntityHealth(GetPlayerPed(-1), 0)
    TriggerEvent('chat:addMessage', {
        args = { 'Kill Command RUN !~' }
    })
end)

RegisterCommand('drunk', function(source, args, rawCommand)
    local health = GetEntityHealth(GetPlayerPed(-1))
    SetEntityHealth(health -100)
    TriggerEvent('chat:addMessage', {
        args = { health }
    })
end)

RegisterCommand('police', function(source, args, rawCommand)
    SetPedAsCop(GetPlayerPed(-1), false)
    TriggerEvent('chat:addMessage', {
        args = { 'Drunk Command RUN !~' }
    })
end)