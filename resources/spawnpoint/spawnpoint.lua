
local spawnPos = vector3(438.84, -983.65, 30.69)

AddEventHandler('onClientGameTypeStart', function()
    exports.spawnmanager:setAutoSpawnCallback(function()
        exports.spawnmanager:spawnPlayer({
            x = spawnPos.x,
            y = spawnPos.y,
            z = spawnPos.z,
            model = 's_m_m_security_01'
        }, function()
            -- TriggerEvent('chat:addMessage', {
            --     args = { 'Welcome to the party!~' }
            -- })
        end)
    end)

    exports.spawnmanager:setAutoSpawn(true)
    exports.spawnmanager:forceRespawn()
end)
AddEventHandler("playerSpawned", function()
    Citizen.CreateThread(function()
  
      local player = PlayerId()
      local playerPed = GetPlayerPed(-1)
  
      -- Enable pvp
      NetworkSetFriendlyFireOption(true)
      SetCanAttackFriendly(playerPed, true, true)
  
    end)
  end)